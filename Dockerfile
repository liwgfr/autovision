FROM ubuntu:20.10
LABEL maintainer="tdoktora@inbox.ru"
ENV ADMIN="binocla"
ENV PATH="/root/miniconda3/bin:${PATH}"
ARG PATH="/root/miniconda3/bin:${PATH}"
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get upgrade -y && apt-get -y install locales && apt-get install -q -y openjdk-15-jdk && apt-get install openjdk-15-jre && apt-get -y install wget && apt-get -y install maven && apt-get clean;
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
RUN wget \
    https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && mkdir /root/.conda \
    && bash Miniconda3-latest-Linux-x86_64.sh -b \
    && rm -f Miniconda3-latest-Linux-x86_64.sh
RUN conda --version
RUN java --version
RUN python --version
COPY ./ ./
RUN conda env create -f /environment.yml
RUN conda init powershell && eval "$(conda shell.bash hook)" && conda activate autovision
RUN which python
RUN which conda
RUN conda list
RUN mvn -version
RUN ls
RUN mvn dependency:go-offline && mvn package
# && mvn exec:java -Dexec.mainClass="Main"
# EXPOSE 8080
# ENTRYPOINT ["java", "-jar", "/target/autovision-1.0-SNAPSHOT.jar"]




