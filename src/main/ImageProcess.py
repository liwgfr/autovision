#!/usr/bin/env python
# coding: utf-8
import functools
import os

import matplotlib.pylab as plt
import numpy as np
import tensorflow as tf
import tensorflow_hub as hub
from matplotlib import gridspec

print("TF Version: ", tf.__version__)
print("TF-Hub version: ", hub.__version__)
print("Eager mode enabled: ", tf.executing_eagerly())
print("GPU available: ", tf.test.is_gpu_available())


def crop_center(image):
    shape = image.shape
    new_shape = min(shape[1], shape[2])
    offset_y = max(shape[1] - shape[2], 0) // 2
    offset_x = max(shape[2] - shape[1], 0) // 2
    image = tf.image.crop_to_bounding_box(
        image, offset_y, offset_x, new_shape, new_shape)
    return image


@functools.lru_cache(maxsize=None)
def load_image(image_url, image_size=(256, 256)):
    image_path = tf.keras.utils.get_file(os.path.basename(image_url)[-128:], image_url)

    img = plt.imread(image_path).astype(np.float32)[np.newaxis, ...]
    if img.max() > 1.0:
        img = img / 255.
    if len(img.shape) == 3:
        img = tf.stack([img, img, img], axis=-1)
    img = crop_center(img)
    img = tf.image.resize(img, image_size, preserve_aspect_ratio=True)
    return img


def show_n(images, titles=('',)):
    n = len(images)
    image_sizes = [image.shape[1] for image in images]
    w = (image_sizes[0] * 6) // 320
    plt.figure(figsize=(w * n, w))
    gs = gridspec.GridSpec(1, n, width_ratios=image_sizes)
    for i in range(n):
        plt.subplot(gs[i])
        plt.imshow(images[i][0], aspect='equal')
        plt.axis('off')
        plt.title(titles[i] if len(titles) > i else '')
    plt.show()


path = 'photos/' # global path
print(path)

content_image_url = path + '0.jpg'  # @param {type:"string"}
print(content_image_url)
style_urls = dict(
    VanGoghStyle='https://upload.wikimedia.org/wikipedia/commons/thumb/e/ea/Van_Gogh_-_Starry_Night_-_Google_Art_Project.jpg/1024px-Van_Gogh_-_Starry_Night_-_Google_Art_Project.jpg',
    FireStyle='https://upload.wikimedia.org/wikipedia/commons/3/36/Large_bonfire.jpg',
    WavesStyle='https://upload.wikimedia.org/wikipedia/commons/0/0a/The_Great_Wave_off_Kanagawa.jpg',
    BlakeStyle='https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Reddragon.jpg/696px-Reddragon.jpg',
    MondrianStyle='https://upload.wikimedia.org/wikipedia/commons/6/64/Piet_Mondrian%2C_1908-10%2C_Evening%3B_Red_Tree_%28Avond%3B_De_rode_boom%29%2C_oil_on_canvas%2C_70_x_99_cm%2C_Gemeentemuseum_Den_Haag.jpg'
)
with open('1.txt', 'r') as reader:
    string = reader.read().replace('\n', '')
print(string)
print(string.__class__)
style_image_url = style_urls.get(string)  # @param {type:"string"}
print(style_image_url)
print(style_urls.get(str))
with open('2.txt') as f:
    lines = f.read().replace('\n', '').split(' ')
print(lines)
print(lines[0])
print(lines[1])
output_image_size_width = int(lines[0])  # @param {type:"integer"}
output_image_size_height = int(lines[1])  # @param {type:"integer"}

content_img_size = (output_image_size_width, output_image_size_height)

style_img_size = (256, 256)
image_path = content_image_url
print(image_path)

img = plt.imread(image_path).astype(np.float32)[np.newaxis, ...]
if img.max() > 1.0:
    img = img / 255.
if len(img.shape) == 3:
    img = tf.stack([img, img, img], axis=-1)
# img = crop_center(img)
img = tf.image.resize(img, content_img_size, preserve_aspect_ratio=True)

content_image = img
style_image = load_image(style_image_url, style_img_size)
style_image = tf.nn.avg_pool(style_image, ksize=[3, 3], strides=[1, 1], padding='SAME')
# show_n([style_image])
# print([style_image])


hub_handle = 'https://tfhub.dev/google/magenta/arbitrary-image-stylization-v1-256/2'
hub_module = hub.load(hub_handle)

outputs = hub_module(tf.constant(content_image), tf.constant(style_image))
stylized_image = outputs[0]

# show_n([stylized_image])
b = tf.squeeze([stylized_image])
# print(b) # Tensor itself

tf.keras.preprocessing.image.save_img(path + '1.jpg', b)
print('true')


# content_image_size = 384
# style_image_size = 256
# style_images = {k: load_image(v, (style_image_size, style_image_size)) for k, v in style_urls.items()}
# style_images = {k: tf.nn.avg_pool(style_image, ksize=[3, 3], strides=[1, 1], padding='SAME') for k, style_image in
#                style_images.items()}

# content_name = 'tuebingen'
# style_name = 'van_gogh_starry_night'

# stylized_image = hub_module(tf.constant(content_images[content_name]), tf.constant(style_images[style_name]))[0]

# show_n([stylized_image])
