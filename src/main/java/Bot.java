import lombok.SneakyThrows;
import org.apache.commons.exec.*;
import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.PhotoSize;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class Bot extends TelegramLongPollingBot {
    public int cnt = 0;
    @Override
    public String getBotUsername() {
        return "Binocla_bot";
    }
    private Update local_update;
    private String chat_id;
    @Override
    public String getBotToken() {
        return "1636028153:AAGatSew00Edo3bwuheF1mX6pAsOh1TFwlY";
    }
    @SneakyThrows
    public void proceedPhoto(@NotNull Update update, String chat_id) {
        List<PhotoSize> photos = update.getMessage().getPhoto();
        photos.sort(Comparator.comparing(PhotoSize::getFileSize).reversed());
        System.out.println(photos);
        fileWriter(photos.get(0).getWidth() + " " + photos.get(0).getHeight(), 2);
        GetFile getFile = new GetFile();
        getFile.setFileId(photos.get(0).getFileId());
        String filePath = execute(getFile).getFilePath();
        downloadFile(filePath, new File("photos/" + 0 + ".jpg"));
        SendPhoto msg = new SendPhoto();
        msg.setChatId(chat_id);
        // Runtime.getRuntime().exec("/usr/bin/xterm");
        //Process p = Runtime.getRuntime().exec("bash -c python /home/binocla/IdeaProjects/autovision/src/main/ImageProcess.py");
        //p.waitFor();
        File f = new File("src/main/ImageProcess.py");
        String absolutePath = f.getAbsolutePath();
        System.out.println(absolutePath);
        // String condaPath = new File(absolutePath).getParentFile().getParentFile().getParentFile().getParentFile().getParentFile().getAbsolutePath() + "/"; // For Local
        // String condaPath = new File(absolutePath).getParentFile().getAbsolutePath() + "/"; // For Docker
        // System.out.println(condaPath);
        //condaPath = new File(absolutePath).getParentFile().getParentFile().getAbsolutePath() + "/"; // For Docker
        // System.out.println("2 " + condaPath);
        // System.out.println(condaPath);

        // CommandLine cmdLine = new CommandLine(condaPath + "anaconda3/bin/python"); // For Local
         CommandLine cmdLine = new CommandLine("/root/miniconda3/envs/autovision/bin/python"); // For Docker
         cmdLine.addArgument("/src/main/ImageProcess.py"); // For Docker
        // cmdLine.addArgument("/home/binocla/IdeaProjects/autovision/src/main/ImageProcess.py"); // For Local
        DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler(); // For Local
        ExecuteWatchdog watchdog = new ExecuteWatchdog(3*60*1000);
        Executor executor = new DefaultExecutor();
        executor.setExitValue(1);
        executor.setWatchdog(watchdog);
        executor.execute(cmdLine, resultHandler);

        resultHandler.waitFor();
        msg.setPhoto(new InputFile(new File("photos/" + 1 + ".jpg")));

        try {
            execute(msg);
            File x = new File("photos/1.jpg");
            if (x.delete()) {
                System.out.println("Deleted successfully neural file");
            }
            x = new File("photos/0.jpg");
            if (x.delete()) {
                System.out.println("Deleted successfully origin file");
            }
            x = new File("1.txt");
            if (x.delete()) {
                System.out.println("Successfully deleted text temp file");
            }
            x = new File("2.txt");
            if (x.delete()) {
                System.out.println("Successfully deleted SIZE file");
            }
            cnt++;
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
    @SneakyThrows
    @Override
    public void onUpdateReceived(Update update) {
        String user_choice;
        if (update.hasCallbackQuery()) {
            SendMessage m = new SendMessage();
            user_choice = update.getCallbackQuery().getData();
            fileWriter(user_choice, 1);
            m.setText(update.getCallbackQuery().getData());
            m.setChatId(String.valueOf(update.getCallbackQuery().getMessage().getChatId()));
            execute(m);
            proceedPhoto(local_update, chat_id);
        }

        if (update.hasMessage() && update.getMessage().hasPhoto()) {
            chat_id = String.valueOf(update.getMessage().getChatId());
            local_update = update;
            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            InlineKeyboardButton inlineKeyboardButton1 = new InlineKeyboardButton();
            InlineKeyboardButton inlineKeyboardButton2 = new InlineKeyboardButton();
            InlineKeyboardButton inlineKeyboardButton3 = new InlineKeyboardButton();
            InlineKeyboardButton inlineKeyboardButton4 = new InlineKeyboardButton();
            InlineKeyboardButton inlineKeyboardButton5 = new InlineKeyboardButton();


            inlineKeyboardButton1.setText("Волны");
            inlineKeyboardButton1.setCallbackData("WavesStyle");
            inlineKeyboardButton2.setText("Ван Гог");
            inlineKeyboardButton2.setCallbackData("VanGoghStyle");
            inlineKeyboardButton3.setText("Огонь");
            inlineKeyboardButton3.setCallbackData("FireStyle");
            inlineKeyboardButton4.setText("Уильям Блейк");
            inlineKeyboardButton4.setCallbackData("BlakeStyle");
            inlineKeyboardButton5.setText("Пит Мондриан");
            inlineKeyboardButton5.setCallbackData("MondrianStyle");


            ArrayList<InlineKeyboardButton> rowListButton1 = new ArrayList<>();
            ArrayList<InlineKeyboardButton> rowListButton2 = new ArrayList<>();
            ArrayList<InlineKeyboardButton> rowListButton3 = new ArrayList<>();
            ArrayList<InlineKeyboardButton> rowListButton4 = new ArrayList<>();
            ArrayList<InlineKeyboardButton> rowListButton5 = new ArrayList<>();


            rowListButton1.add(inlineKeyboardButton1);
            rowListButton2.add(inlineKeyboardButton2);
            rowListButton3.add(inlineKeyboardButton3);
            rowListButton4.add(inlineKeyboardButton4);
            rowListButton5.add(inlineKeyboardButton5);

            List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
            rowList.add(rowListButton1);
            rowList.add(rowListButton2);
            rowList.add(rowListButton3);
            rowList.add(rowListButton4);
            rowList.add(rowListButton5);


            inlineKeyboardMarkup.setKeyboard(rowList);
            SendMessage message = new SendMessage();
            message.setChatId(chat_id);
            message.setText("Выберите стиль");
            message.setReplyMarkup(inlineKeyboardMarkup);
            execute(message);

        }

        if (update.hasMessage() && update.getMessage().hasText()) {
            String user_first_name = update.getMessage().getChat().getFirstName();
            String user_last_name = update.getMessage().getChat().getLastName();
            long user_id = update.getMessage().getChat().getId();
            String message_text = update.getMessage().getText();
            SendMessage message = new SendMessage();
            message.setChatId(String.valueOf(update.getMessage().getChatId()));
            message.setText("Отправь мне любую фотографию, а я её обработаю!");
            if (message_text.equals("/help")) {
                message.setText("Создатель бота @binocla\nПо всем вопросам пишите ему :з");
            }
            log(user_first_name, user_last_name, Long.toString(user_id), message_text, message_text);
            try {
                execute(message); // Call method to send the message
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }
    public void fileWriter(String s, int num) throws IOException {
        FileWriter fileWriter = new FileWriter(num  + ".txt");
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println(s);
        printWriter.flush();
        printWriter.close();
    }
    private void log(String first_name, String last_name, String user_id, String txt, String bot_answer) {
        System.out.println("\n ----------------------------");
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
        System.out.println("Message from " + first_name + " " + last_name + ". (id = " + user_id + ") \n Text - " + txt);
        System.out.println("Bot answer: \n Text - " + bot_answer);
    }
}
